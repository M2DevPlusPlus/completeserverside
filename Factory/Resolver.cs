﻿using Contract.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Factory
{
    public class Resolver
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static List<Assembly> DLL = new List<Assembly>();
        private static Dictionary<Type, Type> typeDict = new Dictionary<Type, Type>();
        private static Dictionary<Type, Object> instanceDict = new Dictionary<Type, Object>();

        public static object LoadAssemblies(string[] dlls)
        {
            try
            {
                foreach (string file in dlls)
                {
                    var asm = Assembly.LoadFrom(file);
                    DLL.Add(asm);

                };

            }
            catch (Exception ex)
            {
                log.Error("function: Resolver.LoadAssemblies on load Dlls, error: ", ex);

                return ex.Message;
            }
            try
            {
                DLL.ForEach(dll =>
                {
                    var types = dll.GetTypes().ToList();
                    types.ForEach(t =>
                    {
                        if (t.GetCustomAttribute<ClassToMapAttribute>() != null)
                        {
                            typeDict.Add(t.GetCustomAttribute<RegisterAttribute>().Parent, t);
                        }
                    });
                });
            }
            catch (Exception ex)
            {
                log.Error("function: Resolver.LoadAssemblies on load types , error: ", ex);

                return ex.Message;
            }

            return "Success";
        }
        private static Resolver instance = null;
        public static Resolver GetInstance()
        {
            if (instance == null)
            {
                instance = new Resolver();
            }
            return instance;
        }
        public T Resolve<T>() where T : class
        {

            Type ty = typeDict[typeof(T)];

            if (!instanceDict.ContainsKey(ty))
            {
                instanceDict.Add(ty, Activator.CreateInstance(ty));

            }
            return instanceDict[ty] as T;
        }
    }
}
