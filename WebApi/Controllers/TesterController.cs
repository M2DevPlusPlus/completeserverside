﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contract.IBLL;
using Contract.IDBL;
using DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApiSwagger.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TesterController : ControllerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string connectStr;
        public TesterController(string cs)
        {
            connectStr = cs;
        }
        [HttpGet]
        [Route("GetAllSystems")]
        public IActionResult Get()
        {
            var dbl = Factory.Resolver.GetInstance().Resolve<ISystemDBL>();
            dbl.connectStr = connectStr;
            var list = dbl.getSystemsList<SystemDTO>();
            return Ok(list);
        }
        [HttpPost]
        [Route("TestPath")]
        public async Task<IActionResult> TestPathAsync(TestDTO path)
        {

            ITester test = Factory.Resolver.GetInstance().Resolve<ITester>();
            ResponseDTO responses = new ResponseDTO();
            try
            {
                if (path.TypePath == "post")
                {
                    responses = (await test.post(path) as ResponseDTO);
                }
                else if (path.TypePath == "get")
                {
                    responses = (await test.get(path) as ResponseDTO);
                }
                if (path.TypePath == "put")
                {
                    responses = (await test.put(path) as ResponseDTO);
                }
                else if (path.TypePath == "delete")
                {
                    responses = (await test.delete(path) as ResponseDTO);

                }
            }
            catch (Exception ex)
            {

                log.Error("function: TesterController.TestPathAsync , error: ", ex);

            }
            return Ok(responses);
        }

    }
}

