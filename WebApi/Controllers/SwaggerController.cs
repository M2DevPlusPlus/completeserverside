﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Contract;
using Contract.IBLL;
using Contract.IDBL;
using DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace WebApiSwagger.Controllers
{
    public class Descs
    {
        public Dictionary<string,string> sysDescs { get; set; }
        public Dictionary<string, string> pathName { get; set; }
        public Dictionary<string, string> pathDesc { get; set; }
        public Dictionary<string, string> pathReturns { get; set; }
        public Dictionary<string, string> fieldDesc { get; set; }

    }
    public class Api
    {
        public string url { get; set; }
        public string swaggerUrl { get; set; }
        public string desc { get; set; }
    }

    [Route("api/[controller]")]
    [ApiController]
    public class SwaggerController : ControllerBase
    {
        string connectStr;
        public SwaggerController(string cs)
        {
            connectStr = cs;
        }
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        private async Task<SystemDTO> swagger(string url, IConvertJson convertJson)
        {
            var res = Factory.Resolver.GetInstance();

            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            SystemDTO systemSwagger = convertJson.deserializeToSystemDTO(responseBody, url) as SystemDTO;
            return systemSwagger;
        }

        private async Task<JObject> getComponentsOfApiAsync(Api api, IConvertJson convertJson)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(api.swaggerUrl);
            string responseBody = await response.Content.ReadAsStringAsync();
            JObject res = convertJson.GetComponentsOfApi(responseBody);
            return res;
        }

        [HttpPost]
        [Route("GetApi")]
        public IActionResult GetApi(Api api)
        {

            try
            {
                var res = Factory.Resolver.GetInstance();
                IMerge mergeDBAndSwagger = res.Resolve<IMerge>();
                mergeDBAndSwagger.connectStr = connectStr;
                IConvertJson converter = res.Resolve<IConvertJson>();
                ISystemDBL sysDbl = res.Resolve<ISystemDBL>();
                sysDbl.connectStr = connectStr;
                Task<SystemDTO> t1 = Task.Run(() => swagger(api.swaggerUrl, converter));
                Task<SystemDTO> t2 = Task.Run(() => sysDbl.GetWholeSystem<SystemDTO, PathDTO, FieldDTO>(api.url));
                Task<JObject> t3 = Task.Run(() => getComponentsOfApiAsync(api, converter));
                List<Task> tasks = new List<Task>() { t1, t2, };
                Task.WaitAll(tasks.ToArray());
                JObject comp = t3.Result;
                SystemDTO systemSwagger = t1.Result;
                SystemDTO systemDB = t2.Result;
                if (api.desc != null && api.desc != "")
                    systemSwagger.description = api.desc;
                SystemDTO result = mergeDBAndSwagger.mergeDBAndSwaggerJson(systemSwagger, systemDB) as SystemDTO;

                List<dynamic> ret = new List<dynamic>() { result, comp };
                return Ok(ret);
            }
            catch (Exception ex)
            {
                log.Error("function: SwaggerController.GetApi, error: ", ex);

                return StatusCode(500);
            }



        }

        [HttpPost]
        [Route("{action}")]
        public IActionResult ChangeDesc([FromBody]Descs obj)
        {
            IMainDBL DBL = Factory.Resolver.GetInstance().Resolve<IMainDBL>();
            DBL.connectStr = connectStr;
            var sysDescs = obj.sysDescs;
            var pathDescs = obj.pathDesc;
            var pathReturns = obj.pathReturns;
            var fieldDescs = obj.fieldDesc;
            var pathName = obj.pathName;
            var isSucc = DBL.UpdateDescriptions(sysDescs, pathDescs, pathReturns, pathName, fieldDescs);
            return Ok(isSucc);
        }

        [HttpPost]
        [Route("GetComponentsOfApi")]
        public async Task<IActionResult> GetComponentsOfApi(Api api)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(api.swaggerUrl);
            string responseBody = await response.Content.ReadAsStringAsync();
            IConvertJson convertJSON = Factory.Resolver.GetInstance().Resolve<IConvertJson>();
            JObject res = convertJSON.GetComponentsOfApi(responseBody);
            return Ok(res);
        }

        [HttpGet]
        [Route("{action}/{id}")]
        public IActionResult DeleteSystem(string id)
        {
            var sysDBL = Factory.Resolver.GetInstance().Resolve<ISystemDBL>();
            return Ok( sysDBL.DeleteWholeSystem(id));

        }


    }
}
