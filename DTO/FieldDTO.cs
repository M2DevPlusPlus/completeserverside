﻿using Contract;
using Contract.Attributes;
using Contract.IDTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class FieldDTO:IField
    {
        public string fieldId { get; set; }

        public string pathId { get; set; }
        public string systemId { get; set; }
        public string fieldName { get; set; }
        public string fieldType { get; set; }
        public string fieldIn { get; set; }
        public string description { get; set; }
        public bool require { get; set; }
    }
}
