﻿using Contract;
using Contract.Attributes;
using Contract.IDTO;
using MyJsonConvert;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace DTO
{
     public class SystemDTO:ISystem
    {
        public string systemId { get; set; }
        public string url { get; set; }
        public string description { get; set; }

        public string swaggerUrl { get; set; }
        [NotInclude]
        [JsonConverter(typeof(ConcreteConverter<PathDTO>))]
        public List<IPath> pathTbl { get; set; }


    }
}
