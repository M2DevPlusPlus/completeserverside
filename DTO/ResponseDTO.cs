﻿using Contract.IDTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class ResponseDTO:IResponse
    {
        public string RequwstURL { get; set; }
        public string ResponseBody { get; set; }
        public string ResponseCode { get; set; }
        public Dictionary<string, List<string>> ResponseHeaders { get; set; }
    }
}
