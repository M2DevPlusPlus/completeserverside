﻿using Contract.IDTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class TestDTO:ITest
    {
        public string Url { get; set; }
        public Dictionary<string, string> Properties { get; set; }
        public string RequestBody { get; set; }
        public string TypePath { get; set; }
    }
}
