﻿using Contract;
using Contract.Attributes;
using Contract.IDTO;
using MyJsonConvert;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace DTO
{
    public class PathDTO:IPath
    {
        public string pathId { get; set; }
        public string systemId { get; set; }
        public string url { get; set; }
        public string pathReturn { get; set; }

        public string description { get; set; }

        public string httpType { get; set; }
        public string component { get; set; }
        public string pathName { get; set; }


     
        [NotInclude]
        [JsonConverter(typeof(ConcreteConverter<FieldDTO>))]

        public List<IField> fieldTbl { get; set; }
    }
}
