﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DAL
{
    public class SuperDal
    { private string connectStr;

        public SuperDal(string cs)
        {
            connectStr = cs;
        }
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public DataSet get(List<SqlParameter> parameters, string nameSP)
        {
            try
            {
               DB db = new DB(connectStr);
                db.openDB();
                SqlCommand cmd = new SqlCommand(nameSP, db.dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                if (cmd.Parameters.Count > 1)
                {
                    for (int i = 0; i < parameters.Count(); i++)
                    {
                        cmd.Parameters[i + 1].Value = parameters[i].Value;
                    }
                }
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.dbConn.Close();
                return ds;
            }
            catch (Exception ex)
            {

                log.Error("function: SuperDal.get, error: ", ex);

                return null;
            }
        }

        public bool Delete(List<SqlParameter> parameters, string nameSP)
        {
            try
            {
                DB db;
                db = new DB(connectStr);
                db.openDB();
                SqlCommand cmd = new SqlCommand(nameSP, db.dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < parameters.Count(); i++)
                {
                    cmd.Parameters[i + 1].Value = parameters[i].Value;
                }
                var temp = cmd.ExecuteNonQuery();
                db.dbConn.Close();
                return true;
            }
            catch (Exception ex)
            {
                log.Error("function: SuperDal.Delete, error: ", ex);

                //display error message
                return false;
            }
        }


        public bool Add(List<SqlParameter> parameters, string nameSP)
        {
            try
            {
                DB db;
                db = new DB(connectStr);
                db.openDB();
                SqlCommand cmd = new SqlCommand(nameSP, db.dbConn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddRange(parameters.ToArray());
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.dbConn.Close();
                return true;
            }
            catch (Exception ex)
            {
                log.Error("function: SuperDal.Add, error: ", ex);

                //display error message
                return false;
            }
        }

        public bool Update(List<SqlParameter> parameters, string nameSP)
        {
            try
            {
                DB db;
                db = new DB(connectStr);
                db.openDB();
                SqlCommand cmd = new SqlCommand(nameSP, db.dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddRange(parameters.ToArray());
                var temp = cmd.ExecuteNonQuery();
                db.dbConn.Close();
                return true;
            }
            catch (Exception ex)
            {
                log.Error("function: SuperDal.Update, error: ", ex);

                return false;
            }
        }
        public void AddList(DataTable data, string SPName, string TypeName)
        {
            try
            {
                DB db;
                db = new DB(connectStr);
                db.openDB();
                SqlCommand cmd = new SqlCommand(SPName, db.dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter tvparam = cmd.Parameters.AddWithValue("@List", data);
                tvparam.SqlDbType = SqlDbType.Structured;
                tvparam.TypeName = TypeName;
                var temp = cmd.ExecuteNonQuery();
                db.dbConn.Close();
                Console.WriteLine(temp);
            }
            catch (Exception ex)
            {
                log.Error("function: SuperDal.AddList, error: ", ex);

                Console.WriteLine(ex);
            }

        }
    }
}
