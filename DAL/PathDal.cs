﻿using Contract;
using Contract.Attributes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DAL
{

    public class PathDal
    {
        public PathDal(string cs)
        {
            connectStr = cs;
        }

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string connectStr;

        public bool AddPath(List<SqlParameter> parameters)
        {
            return new SuperDal(connectStr).Add(parameters, "AddPath");
        }

        public bool DeletePathById(List<SqlParameter> parameters)
        {
            try
            {
                new SuperDal(connectStr).Delete(parameters, "DeletePath");
                return true;
            }
            catch (Exception ex)
            {
                log.Info("function: PathDal.DeletePathById, error: ", ex);

                return false;
            }
        }

        public bool DeletePathsBySysId(List<SqlParameter> parameters)
        {
            try
            {
                new SuperDal(connectStr).Delete(parameters, "DeletePathBySysId");
                return true;
            }
            catch (Exception ex)
            {
                log.Info("function: PathDal.DeletePathBySysId, error: ", ex);

                return false;
            }
        }


        public DataSet GetPathById(List<SqlParameter> parameters)
        {
            return new SuperDal(connectStr).get(parameters, "GetPath");
        }

        public DataSet GetPathsBySystemId(List<SqlParameter> parameters)
        {
            return new SuperDal(connectStr).get(parameters, "GetPathsPerSystem");
        }

        public bool UpdatePath(List<SqlParameter> parameters)
        {
            try
            {
                return new SuperDal(connectStr).Update(parameters, "UpdatePath");

            }
            catch (Exception ex)
            {
                log.Info("function: PathDal.UpdatePath, error: ", ex);

                return false;
            }
        }

        public void AddPathes(DataTable pathesList)
        {
            new SuperDal(connectStr).AddList(pathesList, "AddPathesList", "dbo.PathList");
        }

        public void DeletePathes(DataTable fieldsData)
        {
            new SuperDal(connectStr).AddList(fieldsData, "DeletePathes", "dbo.IDS");
        }
    }

}
