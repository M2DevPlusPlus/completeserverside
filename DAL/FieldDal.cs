﻿using Contract;
using Contract.Attributes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DAL
{
    public class FieldDal
    {
        private string connectStr;

        public FieldDal(string connectStr)
        {
            this.connectStr = connectStr;
        }
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public DataSet GetFieldsBySystemId(List<SqlParameter> parameters)
        {
            return new SuperDal(connectStr).get(parameters, "GetFieldsPerSystem");
        }
        public bool AddField(List<SqlParameter> parameters)
        {
            return new SuperDal(connectStr).Add(parameters, "AddField");
        }

        public bool DeleteFieldById(List<SqlParameter> parameters)
        {
            try
            {
                new SuperDal(connectStr).Delete(parameters, "DeleteField");
                return true;
            }
            catch (Exception ex)
            {
                log.Info("function: FieldDal.DeleteFieldById, error: ", ex);

                return false;
            }
        }


        public DataSet GetFieldById(List<SqlParameter> parameters)
        {
            return new SuperDal(connectStr).get(parameters, "GetField");
        }

        public DataSet GetFieldsByPathId(List<SqlParameter> p)
        {
            return new SuperDal(connectStr).get(p, "GetFieldsPerPath");
        }

        public bool UpdateField(List<SqlParameter> parameters)
        {
            try
            {
                return new SuperDal(connectStr).Update(parameters, "UpdateField");
            }
            catch (Exception ex)
            {
                log.Info("function: FieldDal.UpdateField, error: ", ex);

                return false;
            }
        }

        public void DeleteFields(DataTable fieldsData)
        {
            new SuperDal(connectStr).AddList(fieldsData, "DeleteFields", "dbo.IDS");

        }

        public void AddFields(DataTable fieldsList)
        {
            new SuperDal(connectStr).AddList(fieldsList, "AddFieldsList", "dbo.FieldList");
        }
    }
}
