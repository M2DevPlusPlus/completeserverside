﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Contract;
using Contract.Attributes;
namespace DAL
{

    public class SystemDal
    {
        private string connectStr;

        public SystemDal(string cs)
        {
            connectStr = cs;
        }
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public DataSet GetSystemById(List<SqlParameter> parameters)
        {
            return new SuperDal(connectStr).get(parameters, "GetSystem");
        }

        public bool DeleteSystem(List<SqlParameter> parameters)
        {

            try
            {
                new SuperDal(connectStr).Delete(parameters, "DeleteSystem");
                return true;
            }
            catch (Exception ex)
            {
                log.Info("function: SystemDal.DeleteSystem, error: ", ex);

                return false;
            }
        }

        public bool UpdateSystem(List<SqlParameter> parameters)
        {
            try
            {
                return new SuperDal(connectStr).Update(parameters, "UpdateSystem");
            }
            catch (Exception ex)
            {
                log.Info("function: SystemDal.UpdateSystem, error: ", ex);

                return false;
            }
        }

        public bool AddSystem(List<SqlParameter> parameters)
        {
            return new SuperDal(connectStr).Add(parameters, "AddSystem");
        }

        public DataSet GetAllSystems()
        {
            return new SuperDal(connectStr).get(null, "GetAllSystems");
        }

    }
}
