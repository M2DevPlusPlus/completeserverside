﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL
{
    public class FactorySwagger
    {
        private static FactorySwagger instance = null;
        public static FactorySwagger GetInstance()
        {
            if (instance == null)
            {
                instance = new FactorySwagger();
            }
            return instance;
        }
        public static Dictionary<string, Type> dict = new Dictionary<string, Type>();
        public FactorySwagger()
        {
            dict.Add("", typeof(TypesParsers.SpecialTypeParser));
            dict.Add("array", typeof(TypesParsers.ArreyTypeParser));
            dict.Add("object", typeof(TypesParsers.DictionaryTypeParser));
        }

        public T Resolve<T>(JToken requestBody, JToken field = null) where T : class, TypesParsers.IParser
        {
            string dataType = "";
            if (requestBody.Value<string>("type") != null && dict.ContainsKey(requestBody.Value<string>("type")))
                dataType = requestBody["type"].ToString();

            Type typeParser = dict[dataType];
            T a = Activator.CreateInstance(typeParser) as T;
            a.requestBody = requestBody;
            a.field = a.initField(field);
            return a;
        }
    }

}
