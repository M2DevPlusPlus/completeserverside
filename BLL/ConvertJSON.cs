﻿using Contract.Attributes;
using Contract.IBLL;
using Contract.IDTO;
using DTO;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    [ClassToMap]
    [Register(typeof(IConvertJson))]
    public class ConvertJSON : IConvertJson
    {
        public JObject GetComponentsOfApi(string responseBody)
        {
            JObject json = JObject.Parse(responseBody);
            var res = getPartionalJson(json, "components");
            if (res == null)
                res = getPartionalJson(json, "definitions");
            var result = JObject.Parse(res.Value.ToString());
            return result;
        }
        private List<IField> getParametersOfPath(JProperty path)
        {
            List<IField> fields = new List<IField>();
            if (path != null)
            {
                var list = path.Values().ToList();
                foreach (var field in list)
                {
                    if (field.Value<string>("in") == "body")
                        fields.Add(getRequestBodyOfPath(field));
                    else
                    {
                        var t = field["schema"] != null ? field["schema"] : field;
                        FieldDTO f = new FieldDTO()
                        {
                            fieldName = field["name"].ToString(),
                            fieldType = t["type"].ToString() == "string" ?
                            GetFormatOfString(field) : t["type"].ToString(),
                            require = field["required"] != null ? isRequired(field["required"]) : false,
                            description = field["description"] != null ? field["description"].ToString() : "",
                            fieldIn = field["in"].ToString()
                        };
                        fields.Add(f);
                    }
                }
            }
            return fields;
        }

        private string GetFormatOfString(JToken field)
        {
            if (field["enum"] != null)
                return "enum:" + field["enum"].ToString();
            if (field["format"] != null && field.Value<string>("format") != "uuid")
                return field["format"].ToString();
            return "string";
        }

        private bool isRequired(JToken require)
        {
            if (require.Value<string>() == "True")
                return true;
            return false;
        }

        private FieldDTO getRequestBodyOfPath(JToken body)
        {
            FactorySwagger instance = FactorySwagger.GetInstance();
            var parser = instance.Resolve<TypesParsers.IParser>(body["schema"], body);
            parser.GetField();
            return parser.field;
        }
        private List<IField> getFieldsOfPath(JProperty parameters, JProperty requestBody)
        {
            List<IField> fields = new List<IField>();
            fields.AddRange(getParametersOfPath(parameters));
            if (requestBody != null)
                fields.Add(getRequestBodyOfPath(requestBody.Value["content"]["application/json"]));
            return fields;
        }
        private List<IPath> getPaths(JProperty SpathsJSON)
        {
            JObject pathsJSON = JObject.Parse(SpathsJSON.Value.ToString());
            List<IPath> LPaths = new List<IPath>();
            var listJson = pathsJSON.Properties().ToList();
            foreach (var path in listJson)
            {
                JObject pathObj = JObject.Parse(path.Value.ToString());
                foreach (var func in pathObj.Properties().ToList())
                {
                    if (func.Name == "get" || func.Name == "post" || func.Name == "put" || func.Name == "delete")
                    {
                        var funcObj = JObject.Parse(func.Value.ToString());
                        PathDTO p = new PathDTO();
                        p.url = path.Name;
                        p.component = getPartionalJson(funcObj, "tags").Value[0].ToString();
                        p.description = getPartionalJson(funcObj, "description") != null ? getPartionalJson(funcObj, "description").Value.ToString() : "";
                        p.httpType = func.Name;
                        var parameters = getPartionalJson(funcObj, "parameters");
                        var requestBody = getPartionalJson(funcObj, "requestBody");
                        p.fieldTbl = getFieldsOfPath(parameters, requestBody);
                        p.pathReturn = "";
                        p.pathName = "";
                        LPaths.Add(p);
                    }
                }
            }
            return LPaths;
        }

        private JProperty getPartionalJson(JObject json, string key)
        {
            var listJson = json.Properties().ToList();
            var jo = listJson.FirstOrDefault(j => j.Name == key);
            return jo;
        }


        public ISystem deserializeToSystemDTO(string swaggerJson, string url)
        {
            JObject json = JObject.Parse(swaggerJson);
            SystemDTO system = new SystemDTO();
            var hostJson = getPartionalJson(json, "host");
            var host = hostJson != null ? hostJson.Value.ToString() : "";

            var schemesJson = getPartionalJson(json, "schemes");
            var schema = schemesJson != null ? (schemesJson.Value[0].ToString() +"://"): "";
            var basePathJson = getPartionalJson(json, "basePath");
            var baseP = basePathJson != null ? basePathJson.Value.ToString() : "";
            system.url =schema!=""?(schema+host+baseP): (url.Split('/')[0] + "//" + url.Split('/')[2]);
            system.description = "";
            JProperty pathsJSON = getPartionalJson(json, "paths");
            system.pathTbl = getPaths(pathsJSON);
            system.swaggerUrl = url;
            return system;


        }
    }
}

