﻿
using Contract.Attributes;
using Contract.IBLL;
using Contract.IDTO;
using DTO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    [ClassToMap]
    [Register(typeof(ITester))]
    public class Tester : ITester
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private HttpResponseMessage result = null;
        static readonly HttpClient client = new HttpClient();

        private string GetUrlWithParameters(ITest p1)
        {
            TestDTO path = p1 as TestDTO;
            string qp = "?";
            foreach (var p in path.Properties.Keys)
            {
                if (path.Url.Contains("{" + p + "}"))
                { path.Url = path.Url.Replace("{" + p + "}", path.Properties[p]); }
                else
                    qp += p + "=" + path.Properties[p] + "&";
            }
            qp = qp.TrimEnd(qp[qp.Length - 1]);
            return path.Url + qp;
        }

        private string GetRequestBody(ITest p1)
        {
            TestDTO path = p1 as TestDTO;
            if (path.RequestBody != "" && path.RequestBody != null)
                return path.RequestBody;
            if (path.Properties.Count > 0)
                return path.Properties.First().Value;
            return "";
        }

        private ResponseDTO Exceptions(Exception e, TestDTO path, string nameFunc)
        {
            var res = new ResponseDTO()
            {
                RequwstURL = path.Url,
                ResponseBody = "no content",
                ResponseCode = result != null ? result.StatusCode.ToString() : "false",
                ResponseHeaders = new Dictionary<string, List<string>>()
            };
            res.ResponseHeaders.Add("err", new List<string>() { e.Message });
            log.Debug("function: " + nameFunc + ", error: ", e);
            return res;
        }
        public async Task<IResponse> post(ITest p1)
        {
            TestDTO path = p1 as TestDTO;
            try
            {
                path.Url = GetUrlWithParameters(path);
                JToken obj;
                var requestBody = GetRequestBody(path);
                if (requestBody[0] == '[')
                    obj = JArray.Parse(requestBody);
                else
                    obj = JObject.Parse(requestBody);
                var myContent = JsonConvert.SerializeObject(obj);
                var buffer = Encoding.UTF8.GetBytes(myContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");
                //    HttpResponseMessage result;
                result = client.PostAsync(path.Url, byteContent).Result;
                var response = result.EnsureSuccessStatusCode();
                var res = new ResponseDTO()
                {
                    RequwstURL = response.RequestMessage.RequestUri.AbsoluteUri,
                    ResponseCode = response.StatusCode.ToString(),
                    ResponseHeaders = new Dictionary<string, List<string>>(),
                    ResponseBody = await result.Content.ReadAsStringAsync()
                };
                response.Headers.ToList().ForEach(h => res.ResponseHeaders.Add(h.Key, h.Value.ToList()));
                return res;
            }
            catch (Exception e)
            {
                return Exceptions(e, path, "post");
            }
        }

        public async Task<IResponse> get(ITest p1)
        {
            TestDTO path = p1 as TestDTO;
            try
            {
                path.Url = GetUrlWithParameters(path);
                result = client.GetAsync(path.Url).Result;
                var response = result.EnsureSuccessStatusCode();
                var res = new ResponseDTO()
                {
                    RequwstURL = response.RequestMessage.RequestUri.AbsoluteUri,
                    ResponseCode = response.StatusCode.ToString(),
                    ResponseHeaders = new Dictionary<string, List<string>>(),
                    ResponseBody = await result.Content.ReadAsStringAsync()
                };
                response.Headers.ToList().ForEach(h => res.ResponseHeaders.Add(h.Key, h.Value.ToList()));
                return res;
            }
            catch (Exception e)
            {

                return Exceptions(e, path, "get");
            }
        }

        public async Task<IResponse> put(ITest p1)
        {
            TestDTO path = p1 as TestDTO;
            try
            {
                path.Url = GetUrlWithParameters(path);
                JToken obj;
                var requestBody = GetRequestBody(path);
                if (requestBody[0] == '[')
                    obj = JArray.Parse(requestBody);
                else
                    obj = JObject.Parse(requestBody);
                var myContent = JsonConvert.SerializeObject(obj);
                var buffer = Encoding.UTF8.GetBytes(myContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");
                result = client.PostAsync(path.Url, byteContent).Result;
                var response = result.EnsureSuccessStatusCode();
                var res = new ResponseDTO()
                {
                    RequwstURL = response.RequestMessage.RequestUri.AbsoluteUri,
                    ResponseCode = response.StatusCode.ToString(),
                    ResponseHeaders = new Dictionary<string, List<string>>(),
                    ResponseBody = await result.Content.ReadAsStringAsync()
                };
                response.Headers.ToList().ForEach(h => res.ResponseHeaders.Add(h.Key, h.Value.ToList()));
                return res;
            }
            catch (Exception e)
            {
                return Exceptions(e, path, "put");
            }
        }
        public async Task<IResponse> delete(ITest p1)
        {
            TestDTO path = p1 as TestDTO;
            try
            {
                path.Url = GetUrlWithParameters(path);
                result = client.DeleteAsync(path.Url).Result;
                var response = result.EnsureSuccessStatusCode();
                var res = new ResponseDTO()
                {
                    RequwstURL = response.RequestMessage.RequestUri.AbsoluteUri,
                    ResponseCode = response.StatusCode.ToString(),
                    ResponseHeaders = new Dictionary<string, List<string>>(),
                    ResponseBody = await result.Content.ReadAsStringAsync()
                };
                response.Headers.ToList().ForEach(h => res.ResponseHeaders.Add(h.Key, h.Value.ToList()));
                return res;
            }
            catch (Exception e)
            {
                return Exceptions(e, path, "delete");
            }
        }


    }
}