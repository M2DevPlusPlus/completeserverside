﻿using DTO;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.TypesParsers
{
    class DictionaryTypeParser : Parsers
    {
        public override void GetField()
        {
            field.fieldType = "dictionary <" + getType(requestBody["additionalProperties"]) + ">";
        }
    }
}
