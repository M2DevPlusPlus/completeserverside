﻿using DTO;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.TypesParsers
{
    public interface IParser
    {
        public JToken requestBody { get; set; }
        public FieldDTO field { get; set; }
        public void GetField();
        public string getType(JToken j);
        public FieldDTO initField(JToken f);
    }

}
