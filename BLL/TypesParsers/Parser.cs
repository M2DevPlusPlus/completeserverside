﻿using DTO;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.TypesParsers
{
    public abstract class Parsers : IParser
    {

        public JToken requestBody { get; set; }
        public FieldDTO field { get; set; }
        public abstract void GetField();

        public string getType(JToken j)
        {
            if (j["$ref"] != null)
                return j["$ref"].ToString().Split('/').Last();
            return j["type"].ToString() == "string" ? GetFormatOfString(j) : j["type"].ToString();
        }
        public FieldDTO initField(JToken f)
        {
            return new FieldDTO()
            {
                fieldName = f["name"] != null ? f["name"].ToString() : "requestBody",
                description = f["description"] != null ? f["description"].ToString() : "",
                require = true,
                fieldIn="body"
            };
        }

        private string GetFormatOfString(JToken field)
        {
            if (field["enum"] != null)
                return "enum";
            if (field["format"] != null && field.Value<string>("format") != "uuid")
                return field["format"].ToString();
            return "string";
        }
    }
}
