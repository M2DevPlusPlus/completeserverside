﻿using DTO;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.TypesParsers
{
    class ArreyTypeParser : Parsers
    {
        public override void GetField()
        {
            var s = requestBody["items"];
            field.fieldType = "array <" + base.getType(s) + ">";
        }
    }
}
