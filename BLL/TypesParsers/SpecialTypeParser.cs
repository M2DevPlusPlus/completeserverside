﻿using DTO;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.TypesParsers
{
    class SpecialTypeParser : Parsers
    {

        public override void GetField()
        {
            field.fieldType = getType(requestBody);
        }
    }
}
