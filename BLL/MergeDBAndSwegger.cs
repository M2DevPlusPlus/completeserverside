﻿using Contract.Attributes;
using Contract.IBLL;
using Contract.IDBL;
using Contract.IDTO;
using DTO;
using Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    [ClassToMap]
    [Register(typeof(IMerge))]
    public class MergeDBAndSwagger : IMerge
    {
        public string connectStr { get; set; }

        public ISystem mergeDBAndSwaggerJson(ISystem s1, ISystem s2)
        {
            List<IPath> pathsToAdd = new List<IPath>();
            SystemDTO SysToAdd = new SystemDTO();
            List<IField> fieldsToAdd = new List<IField>();

            SystemDTO systemSwagger = s1 as SystemDTO;
            SystemDTO systemDB = s2 as SystemDTO;
            List<IPath> pathsToDelete = new List<IPath>();
            List<IField> fieldsToDelete = new List<IField>();
            var factory = Factory.Resolver.GetInstance();
            var SysDBL = factory.Resolve<ISystemDBL>();
            SysDBL.connectStr = connectStr;
            var pathDBL = factory.Resolve<IPathDBL>();
            pathDBL.connectStr = connectStr;
            var fieldDBL = factory.Resolve<IFieldDBL>();
            fieldDBL.connectStr = connectStr; List<PathDTO> tempPaths;
            List<FieldDTO> tempFields;

            if (systemDB.systemId != null)
            {
                systemSwagger.description = systemDB.description != "" ? systemDB.description : systemSwagger.description;
                systemSwagger.systemId = systemDB.systemId;
                systemSwagger.swaggerUrl = systemDB.swaggerUrl;
                tempPaths = new List<PathDTO>();
                if (systemDB.pathTbl != null && systemDB.pathTbl.Count > 0)
                {

                    tempPaths.AddRange(systemDB.pathTbl.Select(p => p as PathDTO));
                    systemSwagger.pathTbl.ToList().ForEach(p1 =>
                    {
                        var pathS = p1 as PathDTO;
                        pathS.systemId = systemDB.systemId;
                        tempFields = new List<FieldDTO>();
                        var pathDB = (PathDTO)systemDB.pathTbl.FirstOrDefault(pdb => pathS.url == ((PathDTO)pdb).url && pathS.httpType == ((PathDTO)pdb).httpType);
                        if (pathDB != null)
                        {
                            tempPaths.Remove(tempPaths.FirstOrDefault(pp => pp.pathId == pathDB.pathId));
                            tempFields.AddRange(pathDB.fieldTbl.Select(f => f as FieldDTO));
                            pathS.pathId = pathDB.pathId;
                            pathS.description = pathDB.description != "" ? pathDB.description : pathS.description;
                            pathS.pathReturn = pathDB.pathReturn;
                            pathS.pathName = pathDB.pathName;
                            pathS.fieldTbl.ToList().ForEach(f1 =>
                            {
                                var fieldS = (FieldDTO)f1;
                                fieldS.pathId = pathS.pathId;
                                fieldS.systemId = pathS.systemId;
                                var fieldDB = (FieldDTO)pathDB.fieldTbl.FirstOrDefault(fdb =>
                                ((FieldDTO)fdb).fieldName == fieldS.fieldName &&

                                fieldS.require == ((FieldDTO)fdb).require);
                                if (fieldDB != null)
                                {
                                    tempFields.Remove(tempFields.FirstOrDefault(ff => ff.fieldId == fieldDB.fieldId));
                                    fieldS.fieldId = fieldDB.fieldId;
                                    fieldS.description = fieldDB.description != "" ? fieldDB.description : fieldS.description;
                                }
                                else
                                {
                                    fieldS.fieldId = IdGenerator.GenerateId();
                                    fieldsToAdd.Add(fieldS);
                                }
                            });
                            fieldsToDelete.AddRange(tempFields);
                        }
                        else
                        {
                            pathS.pathName = "";
                            pathS.pathReturn = "";
                            pathS.pathId = IdGenerator.GenerateId();
                            pathS.fieldTbl.ForEach(f =>
                            {
                                f.systemId = pathS.systemId;
                                f.pathId = pathS.pathId;
                                f.fieldId = IdGenerator.GenerateId();
                                fieldsToAdd.Add(f);
                            });
                            pathsToAdd.Add(pathS);
                        }
                    });
                    pathsToDelete.AddRange(tempPaths);
                }
                else
                {
                    systemSwagger.pathTbl.ForEach(p =>
                    {
                        p.systemId = systemDB.systemId;
                        p.pathId = IdGenerator.GenerateId();
                        p.fieldTbl.ForEach(f =>
                        {
                            f.systemId = p.systemId;
                            f.pathId = p.pathId;
                            f.fieldId = IdGenerator.GenerateId();
                            fieldsToAdd.Add(f);
                        });
                    });
                    pathsToAdd.AddRange(systemSwagger.pathTbl);
                }
            }
            else
            {
                systemSwagger.systemId = IdGenerator.GenerateId();
                systemSwagger.pathTbl.ForEach(p =>
                {
                    p.systemId = systemSwagger.systemId;
                    p.pathId = IdGenerator.GenerateId();
                    p.fieldTbl.ForEach(f =>
                    {
                        f.systemId = p.systemId;
                        f.pathId = p.pathId;
                        f.fieldId = IdGenerator.GenerateId();
                        fieldsToAdd.Add(f);
                    });
                });
                SysToAdd = systemSwagger;
            }

            Task.Run(() =>
            {
                if (SysToAdd != null && SysToAdd.systemId != null)
                {
                    SysDBL.connectStr = connectStr;
                    SysDBL.AddWholeSystem<SystemDTO, PathDTO, FieldDTO>(SysToAdd);
                }
                else
                {
                    if (pathsToAdd.Count > 0)
                    {
                        pathDBL.connectStr = connectStr;
                        pathDBL.AddPathes<PathDTO>(pathsToAdd);
                    }
                    if (fieldsToAdd.Count > 0)
                    {
                        fieldDBL.connectStr = connectStr;
                        fieldDBL.AddFields<FieldDTO>(fieldsToAdd);
                    }
                }
                if (fieldsToDelete.Count > 0)
                {
                    fieldDBL.DeleteFields(fieldsToDelete);
                }
                if (pathsToDelete.Count > 0)
                {
                    pathDBL.DeletePathes(pathsToDelete);
                }
            });
            return systemSwagger;
        }
    }
}

