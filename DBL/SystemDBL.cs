﻿using Contract;
using Contract.Attributes;
using Contract.IDBL;
using Contract.IDTO;
using Convert;
using DAL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DBL
{
    [ClassToMap]
    [Register(typeof(ISystemDBL))]
    public class SystemDBL : ISystemDBL
    {
        public string connectStr { get; set; }

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public T1 GetWholeSystem<T1, T2, T3>(string url) where T1 : ISystem where T2 : IPath where T3 : IField
        {
            if (url == null || url == "")
                return Activator.CreateInstance<T1>();
            var sysDal = new SystemDal(connectStr);
            var pathDal = new PathDal(connectStr);
            var fieldDal = new FieldDal(connectStr);
            var conversion = new DataConvert();

            List<SqlParameter> urlParm = conversion.ConvertSingleSimpleTypeToDBset<string>(url);
            T1 system = conversion.ConvertSingleDBsetToDTO<T1>(sysDal.GetSystemById(urlParm));
            if (system.systemId != null)
            {
                var sysIdParm = conversion.ConvertSingleSimpleTypeToDBset<string>(system.systemId);
                var paths = pathDal.GetPathsBySystemId(sysIdParm);
                if (paths != null && paths.Tables != null && paths.Tables.Count > 0)
                {
                    system.pathTbl = conversion.ConvertDBsetToDTO<T2, IPath>(paths);
                    var fields = fieldDal.GetFieldsBySystemId(sysIdParm);
                    if (fields != null && fields.Tables != null && fields.Tables.Count > 0)
                    {
                        var newFields = conversion.ConvertDBsetToDTO<T3, IField>(fields);
                        system.pathTbl.ForEach(p =>
                        {
                            p.fieldTbl = new List<IField>();
                            var temp = newFields.Where(f => f.pathId == p.pathId).ToList();
                            if (temp.Count() > 0)
                            {
                                try
                                {
                                    p.fieldTbl.AddRange(temp);
                                    newFields.RemoveAll(f => f.pathId == p.pathId);
                                }
                                catch (Exception x)
                                {
                                    log.Info("function: SystemDBL.GetWholeSystem, error: ", x);

                                }
                            }
                        });
                    }
                 
                }
            }
            return system;
        }
        public void AddWholeSystem<T1, T2, T3>(T1 system) where T1 : ISystem where T2 : IPath where T3 : IField
        {
            List<IField> fields = new List<IField>();
            var convert = new DataConvert();
            var sysDal = new SystemDal(connectStr);
            sysDal.AddSystem(convert.ConvertDTOToDBset<T1, ISystem>(system));
            new PathDBL(connectStr).AddPathes<T2>(system.pathTbl);
            system.pathTbl.ForEach(p =>
            fields.AddRange(p.fieldTbl)
            );
            new FieldDBL(connectStr).AddFields<T3>(fields);

        }
        public List<T> getSystemsList<T>() where T : ISystem
        {
            DataConvert conversion = new DataConvert();
            var sysDal = new SystemDal(connectStr);
            var list = conversion.ConvertDBsetToDTO<T, ISystem>(sysDal.GetAllSystems()).Cast<T>().ToList();
            return list;
        }

        public bool DeleteWholeSystem(string id)
        {
            var sysDal = new SystemDal(connectStr);
            var idParm = new DataConvert().ConvertSingleSimpleTypeToDBset<string>(id);
            return sysDal.DeleteSystem(idParm);
        }
    }
}
