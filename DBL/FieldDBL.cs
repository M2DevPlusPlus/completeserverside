﻿using Contract;
using Contract.Attributes;
using Contract.IDBL;
using Contract.IDTO;
using Convert;
using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DBL
{
    [ClassToMap]
    [Register(typeof(IFieldDBL))]
    public class FieldDBL : IFieldDBL
    {
        public FieldDBL()
        {

        }
        public FieldDBL(string cs)
        {
            connectStr = cs;
        }
        public string connectStr { get; set; }

        public bool AddField<T>(T field) where T : IField
        {
            var convert = new DataConvert();
            var fieldDal = new FieldDal(connectStr);
            var fieldParm = convert.ConvertDTOToDBset<T, IField>(field);
            return fieldDal.AddField(fieldParm);
        }
        public void DeleteField<T>(T field) where T : IField
        {
            var fieldDal = new FieldDal(connectStr);
            var conversion = new DataConvert();
            var fieldParm = conversion.ConvertSingleSimpleTypeToDBset<string>(field.fieldId);
            fieldDal.DeleteFieldById(fieldParm);
        }

        public void AddFields<T>(List<IField> fields) where T : IField
        {
            DataTable fieldsData = new DataConvert().ConvertList<T, IField>(fields);
            new FieldDal(connectStr).AddFields(fieldsData);
        }

        public void DeleteFields(List<IField> fields)
        {
            List<ID> IDs = new List<ID>();
            DataTable fieldsData = new DataConvert().ConvertSingleSimpleList<string>(fields.Select(f => f.fieldId).ToList(), "id");
            new FieldDal(connectStr).DeleteFields(fieldsData);
        }
    }
}
