﻿using Contract.Attributes;
using Contract.IDBL;
using Contract.IDTO;
using Convert;
using DAL;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DBL
{
    [ClassToMap]
    [Register(typeof(IPathDBL))]
    public class PathDBL : IPathDBL
    {
        public string connectStr { get; set; }
        public PathDBL(string cs)
        {
            connectStr = cs;
        }
        public PathDBL()
        {

        }
        public void AddPath<T>(T path) where T : IPath
        {
            var pathDal = new PathDal(connectStr);
            var conversion = new DataConvert();
            var pathParm = conversion.ConvertDTOToDBset<T, IPath>(path);
            pathDal.AddPath(pathParm);
        }
        public void AddPathes<T>(List<IPath> pathes) where T : IPath
        {
            DataTable PathesList = new DataConvert().ConvertList<T, IPath>(pathes);
            new PathDal(connectStr).AddPathes(PathesList); 


        }


        public void DeletePath<T>(T path) where T : IPath
        {
            var pathDal = new PathDal(connectStr);
            var conversion = new DataConvert();
            var pathParm = conversion.ConvertSingleSimpleTypeToDBset<string>(path.pathId);
            pathDal.DeletePathById(pathParm);
        }
        public void DeletePathes(List<IPath> pathes)
        {
            List<ID> IDs = new List<ID>();
            DataTable fieldsData = new DataConvert().ConvertSingleSimpleList<string>(pathes.Select(p=>p.pathId).ToList(), "id");
            new PathDal(connectStr).DeletePathes(fieldsData);
        }

    }
}
