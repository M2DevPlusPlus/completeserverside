﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBL
{

    public interface IDesc
    {

    }
    public class Api
    {
        public string url { get; set; }
        public string swaggerUrl { get; set; }
    }
    public class SysDesc : IDesc
    {
        public string systemId { get; set; }
        public string description { get; set; }
    }
    public class PathDesc : IDesc
    {
        public string pathId { get; set; }
        public string description { get; set; }
        public string pathReturn { get; set; }
        public string pathName { get; set; }
    }
    public class FieldDesc : IDesc
    {
        public string fieldId { get; set; }
        public string description { get; set; }
    }
    public class ID
    {
        public ID(string id)
        {
            this.id = id;
        }

        public string id { get; set; }
    }

}
