﻿
using Contract.Attributes;
using Contract.IDBL;
using Convert;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBL
{
    [ClassToMap]
    [Register(typeof(IMainDBL))]
    public class MainDBL : IMainDBL
    {
        public string connectStr { get; set; }


        public int UpdateDescriptions(Dictionary<string, string> sysDescs, Dictionary<string, string> pathDescs, Dictionary<string, string> pathReturns, Dictionary<string, string> pathName, Dictionary<string, string> fieldDescs)
        {
            int faileds = 0;
            var sysDal = new SystemDal(connectStr);
            var pathDal = new PathDal(connectStr);
            var fieldDal = new FieldDal(connectStr);
            var convert = new DataConvert();
            if (sysDescs.Count > 0)
            {
                sysDescs.ToList().ForEach(a =>
                {
                    var s = new SysDesc() { systemId = a.Key, description = a.Value };
                    var parm = convert.ConvertDTOToDBset<SysDesc, IDesc>(s);
                    if (!sysDal.UpdateSystem(parm))
                    {
                        faileds++;
                    }
                });
            }
            if (pathDescs.Count > 0)
            {
                pathDescs.ToList().ForEach(a =>
                {
                    var ret = pathReturns.FirstOrDefault(x => x.Key == a.Key).Value;
                    var name = pathName.FirstOrDefault(x => x.Key == a.Key).Value;
                    var p = new PathDesc()
                    {
                        pathId = a.Key,
                        description = a.Value,
                        pathReturn = ret != null ? ret : "",
                        pathName = name != null ? name : ""
                    };
                    var parm = convert.ConvertDTOToDBset<PathDesc, IDesc>(p);
                    if (!pathDal.UpdatePath(parm))
                    {
                        faileds++;
                    }


                });
            }
            if (fieldDescs.Count > 0)
            {
                fieldDescs.ToList().ForEach(a =>
                {
                    var f = new FieldDesc() { fieldId = a.Key, description = a.Value };
                    var parm = convert.ConvertDTOToDBset<FieldDesc, IDesc>(f);
                    if (!fieldDal.UpdateField(parm))
                    {
                        faileds++;
                    }
                });
            }
            return faileds;
        }

    }
}
