﻿using Contract;
using Contract.Attributes;
using Contract.IDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace Convert
{

    public class DataConvert
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public T ConvertSingleDBsetToDTO<T>(DataSet data)
        {
            T temp = Activator.CreateInstance<T>();


            try
            {
                Type typeT = typeof(T);
                PropertyInfo[] properties = typeT.GetProperties();
                if (data.Tables[0].Rows.Count == 0)
                    return temp;
                for (var col = 0; col < data.Tables[0].Columns.Count; col++)
                {
                    properties.FirstOrDefault(p => p.Name == data.Tables[0].Columns[col].ColumnName)
                        .SetValue(temp, data.Tables[0].Rows[0][col]);
                }
            }
            catch (Exception ex)
            {

                log.Error("function: DataConvert.ConvertSingleDBsetToDTO , error: ", ex);
                return temp;
            }

            return temp;
        }

        public List<IT> ConvertDBsetToDTO<T, IT>(DataSet data) where T : IT
        {
            List<IT> listT = new List<IT>();
            T temp = Activator.CreateInstance<T>();
            try
            {
                Type typeT = typeof(T);
                PropertyInfo[] properties = typeT.GetProperties();

                for (int row = 0; row < data.Tables[0].Rows.Count; row++)
                {
                    for (var col = 0; col < data.Tables[0].Columns.Count; col++)
                    {
                        properties.FirstOrDefault(p => p.GetCustomAttribute<NotIncludeAttribute>() == null && p.Name == data.Tables[0].Columns[col].ColumnName)
                            .SetValue(temp, data.Tables[0].Rows[row][col]);
                    }
                    listT.Add(temp);
                    temp = Activator.CreateInstance<T>();
                }
                return listT;

            }
            catch (Exception ex)
            {
                log.Error("function: DataConvert.ConvertDBsetToDTO , error: ", ex);
                return null;
                 
            }

        }



        public List<SqlParameter> ConvertDTOToDBset<T, IT>(IT data) where T : IT
        {
            try
            {

                Type typeT = typeof(T);
                PropertyInfo[] properties = typeT.GetProperties();
                List<SqlParameter> listParam = new List<SqlParameter>();
                foreach (var prop in properties)
                {
                    if ((prop.GetCustomAttribute<IdentityAttribute>() == null ||
                    (prop.GetCustomAttribute<IdentityAttribute>() != null && (int)prop.GetValue(data) != 0))
                    && prop.GetCustomAttribute<NotIncludeAttribute>() == null)
                    {
                        listParam.Add(new SqlParameter("@" + prop.Name, prop.GetValue(data)));
                    }
                }
                return listParam;
            }
            catch (Exception ex)
            {
                log.Error("function: DataConvert.ConvertDTOToDBset , error: ", ex);
                return null;
                 
            }
        }

        public List<SqlParameter> ConvertSimpleTypeToDBset<T>(List<T> data)
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                data.ForEach(s =>
                    listParam.Add(new SqlParameter("", s))
                );
                return listParam;
            }
            catch (Exception ex)
            {
                log.Error("function: DataConvert.ConvertSimpleTypeToDBset , error: ", ex);
                return null;
                 
            }
        }
        public List<SqlParameter> ConvertSingleSimpleTypeToDBset<T>(T data)
        {
            try
            {
                List<SqlParameter> listParam = new List<SqlParameter>();
                listParam.Add(new SqlParameter("", data));
                return listParam;
            }
            catch (Exception ex)
            {
                log.Error("function: DataConvert.ConvertSingleSimpleTypeToDBset , error: ", ex);
                return null;
                 
            }
        }


        public DataTable ConvertList<T, IT>(List<IT> list) where T : IT
        {
            try
            {
                Type typeT = typeof(T);
                PropertyInfo[] properties = typeT.GetProperties();
                DataTable tvp = new DataTable();

                foreach (var prop in properties)
                {

                    if (prop.GetCustomAttribute<NotIncludeAttribute>() == null)
                    {
                        tvp.Columns.Add(new DataColumn(prop.Name, prop.PropertyType));

                    }

                }
                foreach (T obj in list)
                {
                    List<object> parm = new List<object>();
                    foreach (var pr in properties)
                    {
                        if (pr.GetCustomAttribute<NotIncludeAttribute>() == null)
                            parm.Add(pr.GetValue(obj));

                    }
                    tvp.Rows.Add(parm.ToArray());

                }

                return tvp;
            }
            catch (Exception ex)
            {
                log.Error("function: DataConvert.ConvertList , error: ", ex);
                return null;
                 
            }
        }
        public DataTable ConvertSingleSimpleList<T>(List<T> list, string propName)
        {
            try
            {
                DataTable tvp = new DataTable();
                tvp.Columns.Add(new DataColumn(propName, typeof(T)));
                list.ForEach(l => tvp.Rows.Add(l));

                return tvp;
            }
            catch (Exception ex)
            {
                log.Error("function: DataConvert.ConvertSingleSimpleList , error: ", ex);

                return null;
            }
        }
    }
}

