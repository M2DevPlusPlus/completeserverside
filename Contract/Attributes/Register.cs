﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.Attributes
{
    public  class RegisterAttribute:Attribute

    {
        public Type Parent { get; set; }
        public RegisterAttribute()
        {

        }
        public RegisterAttribute(Type Parent)
        {
            this.Parent = Parent;
        }

    }
}
