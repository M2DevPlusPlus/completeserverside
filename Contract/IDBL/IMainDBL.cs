﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.IDBL
{
   public interface IMainDBL
    {
        public string connectStr { get; set; }

        int UpdateDescriptions(Dictionary<string, string> sysDescs, Dictionary<string, string> pathDescs, Dictionary<string, string> pathReturns, Dictionary<string, string> pathName, Dictionary<string, string> fieldDescs);

    }

}
