﻿using Contract.IDTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.IDBL
{
    public interface IPathDBL
    {
        public string connectStr { get; set; }

        void AddPath<T>(T path) where T : IPath;
        void AddPathes<T>(List<IPath> pathes) where T : IPath;

        void DeletePath<T>(T path) where T : IPath;
        void DeletePathes(List<IPath> pathes);

    }
}
