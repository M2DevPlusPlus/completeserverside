﻿using Contract.IDTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.IDBL
{
    public interface IFieldDBL
    {
        public string connectStr { get; set; }

        bool AddField<T>(T field) where T : IField;
        void DeleteField<T>(T field) where T : IField;
        void AddFields<T>(List<IField> fields) where T : IField;
        void DeleteFields(List<IField> fields);

    }
}
