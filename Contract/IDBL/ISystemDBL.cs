﻿using Contract.IDTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.IDBL
{
    public interface ISystemDBL
    {
        public string connectStr { get; set; }

        T1 GetWholeSystem<T1, T2, T3>(string url) where T1 : ISystem where T2 : IPath where T3 : IField;
        void AddWholeSystem<T1, T2, T3>(T1 system) where T1 : ISystem where T2 : IPath where T3 : IField;

        List<T> getSystemsList<T>() where T : ISystem;
        bool DeleteWholeSystem(string id);
    }
}
