﻿using Contract.IDTO;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.IBLL
{
    public interface IConvertJson
    {
        JObject GetComponentsOfApi(string responseBody);
        ISystem deserializeToSystemDTO(string swaggerJson, string url);
    }
}
