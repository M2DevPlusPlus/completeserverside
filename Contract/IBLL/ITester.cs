﻿using Contract.IDTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Contract.IBLL
{
    public interface ITester
    {

        Task<IResponse> post(ITest path);
        Task<IResponse> get(ITest path);
        Task<IResponse> put(ITest path);
        Task<IResponse> delete(ITest path);
    }
}
