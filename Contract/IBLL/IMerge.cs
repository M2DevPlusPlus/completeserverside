﻿using Contract.IDTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.IBLL
{
    public interface IMerge
    {
        public string connectStr { get; set; }
        ISystem mergeDBAndSwaggerJson(ISystem systemSwagger, ISystem systemDB);

    }
}
