﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.IDTO
{
    public interface ISystem
    {
        public string systemId { get; set; }
        List<IPath> pathTbl { get; set; }
    }
}
