﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Contract.IDTO
{


    public interface IPath
    {
        public string systemId { get; set; }
        public string pathId { get; set; }
        
        List<IField> fieldTbl { get; set; }
    }
}
