﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contract.IDTO
{
    public interface IField
    {
        public string systemId { get; set; }
        public string pathId { get; set; }
        public string fieldId { get; set; }
    }
}
