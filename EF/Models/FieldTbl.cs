﻿using Contract;
using System;
using System.Collections.Generic;

namespace EF.Models
{
    public partial class FieldTbl:Iparameter
    {
        public short FieldId { get; set; }
        public short? PathId { get; set; }
        public string FieldName { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }

        public virtual PathTbl Path { get; set; }
    }
}
