﻿using Contract;
using System;
using System.Collections.Generic;

namespace EF.Models
{
    public partial class SystemTbl:Iparameter
    {
        public SystemTbl()
        {
            PathTbl = new HashSet<PathTbl>();
        }

        public short SystemId { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }

        public virtual ICollection<PathTbl> PathTbl { get; set; }
    }
}
