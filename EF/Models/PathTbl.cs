﻿using Contract;
using System;
using System.Collections.Generic;

namespace EF.Models
{
    public partial class PathTbl:Iparameter
    {
        public PathTbl()
        {
            FieldTbl = new HashSet<FieldTbl>();
        }

        public short PathId { get; set; }
        public short? SystemId { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }

        public virtual SystemTbl System { get; set; }
        public virtual ICollection<FieldTbl> FieldTbl { get; set; }
    }
}
