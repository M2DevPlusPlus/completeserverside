﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EF.Models
{
    public partial class CMSContext : DbContext
    {
        public CMSContext()
        {
        }

        public CMSContext(DbContextOptions<CMSContext> options)
            : base(options)
        {
        }

        public virtual DbSet<FieldTbl> FieldTbl { get; set; }
        public virtual DbSet<PathTbl> PathTbl { get; set; }
        public virtual DbSet<SystemTbl> SystemTbl { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=ONT;Database=CMS;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FieldTbl>(entity =>
            {
                entity.HasKey(e => e.FieldId);

                entity.ToTable("Field_Tbl");

                entity.Property(e => e.FieldId).HasColumnName("fieldId");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.FieldName)
                    .HasColumnName("fieldName")
                    .HasMaxLength(50);

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.PathId).HasColumnName("pathId");

                entity.HasOne(d => d.Path)
                    .WithMany(p => p.FieldTbl)
                    .HasForeignKey(d => d.PathId)
                    .HasConstraintName("FK_Field_Tbl_Path_Tbl");
            });

            modelBuilder.Entity<PathTbl>(entity =>
            {
                entity.HasKey(e => e.PathId);

                entity.ToTable("Path_Tbl");

                entity.Property(e => e.PathId).HasColumnName("pathId");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.SystemId).HasColumnName("systemId");

                entity.Property(e => e.Url).HasColumnName("url");

                entity.HasOne(d => d.System)
                    .WithMany(p => p.PathTbl)
                    .HasForeignKey(d => d.SystemId)
                    .HasConstraintName("FK_Path_Tbl_System_Tbl");
            });

            modelBuilder.Entity<SystemTbl>(entity =>
            {
                entity.HasKey(e => e.SystemId);

                entity.ToTable("System_Tbl");

                entity.Property(e => e.SystemId).HasColumnName("systemId");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.Url).HasColumnName("url");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
